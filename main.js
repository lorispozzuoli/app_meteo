prova.onclick = function () {
  let apiKey = "2b92c14829628399b284f518537da60b";
  let city = document.getElementById("weath").value;
  let uri =
    "http://api.openweathermap.org/data/2.5/weather?q=" +
    city +
    "&appid=" +
    apiKey;

  var request = new XMLHttpRequest();
  request.open("GET", uri, true);
  request.onload = function () {
    if (request.status >= 200 && request.status < 400) {
      //convertre tutto in un file json e con main.temp prendo i dati dentro il file
      var data = JSON.parse(this.response);
      meteo = data.weather[0].main;
      var temp = data.main.temp;
      temp = parseInt(temp) - 273; //riporto in gradi centigradi
      if (meteo == "Clear") {
        meteo = "soleggiato";
      } else if (meteo == "Clouds") {
        meteo = "nuvoloso";
      } else {
        meteo = "piovoso";
      }
      ris = document.getElementById("ris").innerHTML =
        "A " + city + " il tempo è " + meteo + " ci sono " + temp + " gradi";
    } else {
      ris = document.getElementById("ris").innerHTML = "Città non trovata";
    }
  };
  request.send();
};
